<?php

namespace z0x\cmd;
class Cmd
{
    public function run($cmd, $stdin = null)
    {
        $io_init = array(
            0 => array("pipe", "r"),  // stdin
            1 => array("pipe", "w"),  // stdout
            2 => array("pipe", "w"),  // stderr
        );
        $process = proc_open($cmd, $io_init, $pipes, null, null);

        fwrite($pipes[0], $stdin);
        fclose($pipes[0]);
        $std_out = stream_get_contents($pipes[1]);
        fclose($pipes[1]);
        $std_err = stream_get_contents($pipes[2]);
        fclose($pipes[2]);
        proc_close($process);

        return [
            "stdout" => $std_out,
            "stderr" => $std_err
        ];

    }
}